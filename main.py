import io
import os
import sys
from collections import defaultdict
from operator import itemgetter
from pathlib import Path
from sys import exit
from xml.sax import ContentHandler
from xml.sax import make_parser

class Handler:
	def __init__(self):
		self.data = \
		{
			"delete": 0,
			"all": 0,
			"activities": 0,
			"students": defaultdict(int),
			"departments": defaultdict(int),
			"subjects": defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(list))))
		}

		self.personalNumber = ""
		self.activity = ""
		self.id = -1
		self.department = ""
		self.subject = ""
		self.kind = ""
		self.term = ""

	#application logic
	def handleEvent(self):
		if self.personalNumber == ""\
				or self.id == -1\
				or self.department == ""\
				or self.subject == ""\
				or self.kind == ""\
				or self.term == "":
			raise OSError()

		studentSubject = self.data["subjects"][self.term][self.department][self.subject][self.kind]
		self.data["all"] += 1

		if self.activity == "insert":
			self.data["activities"] += 1
			self.data["students"][self.personalNumber] += 1
			self.data["departments"][self.department] += 1

			studentSubject.append(self.personalNumber)

		elif self.activity == "delete":
			self.data["activities"] -= 1
			self.data["delete"] += 1

			if self.data["departments"][self.department] <= 1:
				self.data["departments"].pop(self.department, None)
			else:
				self.data["departments"][self.department] -= 1

			if self.data["students"][self.personalNumber] <= 1:
				self.data["students"].pop(self.personalNumber, None)
			else:
				self.data["students"][self.personalNumber] -= 1

			if self.personalNumber in studentSubject:
				studentSubject.remove(self.personalNumber)

		else:
			raise OSError()

		self.personalNumber = ""
		self.activity = ""
		self.id = -1
		self.department = ""
		self.subject = ""
		self.kind = ""
		self.term = ""

class TXTHandler(Handler):
	def __init__(self):
		Handler.__init__(self)

	def handleLine(self, lineString):
		dataList = lineString.strip().split(";")

		if len(dataList) != 7:
			raise OSError()

		self.personalNumber = dataList[0]
		self.activity = dataList[1]
		self.id = dataList[2]
		self.department = dataList[3]
		self.subject = dataList[4]
		self.kind = dataList[5]
		self.term = dataList[6]

		Handler.handleEvent(self)

class XMLHandler(ContentHandler, Handler):
	def __init__(self):
		Handler.__init__(self)
		self.content = ""

	def startElement(self, tag, attributes):
		for attribute, value in attributes.items():
			if attribute == "personalNumber":
				self.personalNumber = value
			elif attribute == "activity":
				self.activity = value
			elif attribute == "tt:id":
				self.id = value
			elif attribute == "kind":
				self.kind = value

	def characters(self, content):
		self.content += content

	def endElement(self, tag):
		if tag == "tt:department":
			self.department = self.content.strip()
		elif tag == "tt:subject":
			self.subject = self.content.strip()
		elif tag == "tt:term":
			self.term = self.content.strip()
		elif tag == "event":
			Handler.handleEvent(self)

		self.content = ""


def printData():
	print("Počet všech předzápisových akcí: ", handler.data["all"])
	print("Počet zrušených akcí (delete): ", handler.data["delete"])
	print("Počet skutečně zapsaných akcí: ", handler.data["activities"])
	print("Počet studentů: ", len(handler.data["students"]))
	print("Počet předmětů: ", numberOfSubjects)
	print("Počet pracovišť: ", len(departmentStudentSubjects))

	actions = list(departmentStudentSubjects.items())
	actions.sort(key = itemgetter(0))
	actions.sort(key = itemgetter(1))
	for j, key in enumerate(actions):
		print(f"{j + 1}. {key[0]}: {key[1]}")

if __name__ == "__main__":
	inputFile = None
	outputFile = None

	#arguments handling
	argvLength = len(sys.argv)
	for i in range(1, argvLength, 2):
		if i + 1 < argvLength:
			file = sys.argv[i + 1]
			if sys.argv[i] == "-i":
				if os.path.isfile(file):
					inputFile = file
				else:
					exit("Chyba I/O!")
			elif sys.argv[i] == "-o":
				try:
					Path(os.path.dirname(file)).mkdir(parents = True, exist_ok = True)
				except OSError:
					exit("Chyba I/O!")
				outputFile = file

	if inputFile is None:
		exit("Chybějící parametry:"
			 "\n-i <cesta ke vstupnímu souboru>"
			 "\n-o <cesta k výstupnímu souboru> (volitelné)")

	#either handler extends Handler class witch contains application logic
	if inputFile.endswith(".xml"):
		handler = XMLHandler()
		parser = make_parser()
		parser.setContentHandler(handler)
		try:
			parser.parse(inputFile)
		except OSError:
			exit("Chyba I/O!")
	else:
		handler = TXTHandler()
		with io.open(inputFile, 'r', encoding = 'utf8') as file: #file will close automatically
			try:
				line = file.readline()
				while line:
					handler.handleLine(line)
					line = file.readline()
			except OSError:
				exit("Chyba I/O!")

	departmentSubjects = defaultdict(int)
	departmentStudentSubjects = defaultdict(int)
	for term in handler.data["subjects"].values():
		for department, subjects in term.items():
			for subject, kinds in subjects.items():
				if len(kinds["Př"]) > 0:
					studentSubjects = kinds["Př"]
				elif len(kinds["Cv"]) > 0:
					studentSubjects = kinds["Cv"]
				elif len(kinds["Se"]) > 0:
					studentSubjects = kinds["Se"]
				else:
					continue
				departmentSubjects[department] += 1
				#convert to set to remove duplicate students
				departmentStudentSubjects[department] += len(set(studentSubjects))

	numberOfSubjects = 0
	for number in departmentSubjects.values():
		numberOfSubjects += number

	#print to output file or to console
	if outputFile:
		try:
			sys.stdout = io.open(outputFile, 'w', encoding = 'utf8')
			printData()
		except OSError:
			exit("Chyba I/O!")
		finally:
			sys.stdout.close()
	else:
		printData()